plugins {
    id("java")
    id("com.github.johnrengelman.shadow") version ("7.1.2")
}

repositories {
    mavenCentral()
}


tasks.jar {
    manifest.attributes["Main-Class"] = "app.AsyncJavaParserLauncher"
}

dependencies {
    implementation("io.vertx:vertx-core:4.3.0")
    implementation("io.vertx:vertx-rx-java3:4.3.0")
    implementation("com.beust:jcommander:1.82")
    implementation("com.github.javaparser:javaparser-symbol-solver-core:3.24.2")
    implementation("org.slf4j:slf4j-api:1.7.36")
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation("io.reactivex.rxjava3:rxjava:3.1.4")
}