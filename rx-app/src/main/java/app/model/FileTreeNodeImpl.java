package app.model;

import javax.swing.*;

public class FileTreeNodeImpl implements FileTreeNode {
    private final FileTreeNodeIcon node;
    private String name;

    private String parentPath;

    public FileTreeNodeImpl(String name, FileTreeNodeIcon node, String parentPath) {
        this.name = name;
        this.node = node;
        this.parentPath = parentPath;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ImageIcon getIconPath() {
        return node.getImageIcon(node);
    }

    @Override
    public String getParentPath() {
        return parentPath;
    }

    @Override
    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    @Override
    public String toString() {
        return "ProjectElement{" +
            "name=" + name +
            ", icon=" + node.toString() +
            '}';
    }
}
