package app.model.visitor_adapters;

import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.fieldPojo.FieldInfoImpl;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

public class FieldVisitor extends VoidVisitorAdapter<List<FieldInfo>> {
    public void visit(FieldDeclaration fd, List<FieldInfo> collector) {
        super.visit(fd, collector);
        FieldInfo fieldInfo = new FieldInfoImpl();
        fieldInfo.setName(fd.toString());
        fieldInfo.setFieldTypeFullName(fd.asFieldDeclaration().toString());
        collector.add(fieldInfo);
    }
}
