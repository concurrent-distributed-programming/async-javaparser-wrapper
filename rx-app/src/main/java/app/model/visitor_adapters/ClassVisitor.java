package app.model.visitor_adapters;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.classPojo.ClassReportImpl;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.List;
import java.util.Optional;

public class ClassVisitor extends VoidVisitorAdapter<List<ClassReport>> {
    public void visit(ClassOrInterfaceDeclaration cd, List<ClassReport> collector) {
        super.visit(cd, collector);
        ClassReport classReport = new ClassReportImpl();
        Optional<String> fullyQualifiedName = cd.getFullyQualifiedName();
        if (fullyQualifiedName.isPresent()) {
            classReport.setInterface(cd.isInterface());
            classReport.setFullClassName(fullyQualifiedName.get());
            collector.add(classReport);
        }
    }
}
