package app.model;

public class AsyncParserModelImpl implements AsyncParserModel {
    private String projectRootPath;
    private int maxDepth;

    public AsyncParserModelImpl(String projectRootPath) {
        this.projectRootPath = projectRootPath;
    }

    @Override
    public String getProjectRootPath() {
        return projectRootPath;
    }

    @Override
    public void setProjectRootPath(String projectRootPath) {
        this.projectRootPath = projectRootPath;
    }

    @Override
    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }
}
