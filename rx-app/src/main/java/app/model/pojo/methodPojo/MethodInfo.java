package app.model.pojo.methodPojo;

public interface MethodInfo {
    String getName();

    void setName(String name);

    int getSrcBeginLine();

    void setSrcBeginLine(int srcBeginLine);

    void setEndBeginLine(int endBeginLine);
}
