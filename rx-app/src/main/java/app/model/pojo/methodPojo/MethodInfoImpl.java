package app.model.pojo.methodPojo;

public class MethodInfoImpl implements MethodInfo {

    private String name;
    private int srcBeginLine;
    private int endBeginLine;

    public MethodInfoImpl() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getSrcBeginLine() {
        return srcBeginLine;
    }

    @Override
    public void setSrcBeginLine(int srcBeginLine) {
        this.srcBeginLine = srcBeginLine;
    }

    @Override
    public void setEndBeginLine(int endBeginLine) {
        this.endBeginLine = endBeginLine;
    }

    @Override
    public String toString() {
        return "MethodInfoImpl{" +
            "name='" + name + '\'' +
            ", srcBeginLine=" + srcBeginLine +
            ", endBeginLine=" + endBeginLine +
            '}';
    }
}
