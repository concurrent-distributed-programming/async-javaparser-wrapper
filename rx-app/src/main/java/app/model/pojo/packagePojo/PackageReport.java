package app.model.pojo.packagePojo;

public interface PackageReport {
    String getFullPackageName();

    void setFullPackageName(String fullPackageName);
}
