package app.model.pojo.fieldPojo;

public interface FieldInfo {
    String getName();

    void setName(String name);

    void setFieldTypeFullName(String fieldTypeFullName);
}
