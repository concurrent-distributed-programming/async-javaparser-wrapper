package app.model.pojo.fieldPojo;

public class FieldInfoImpl implements FieldInfo {

    private String name;
    private String fieldTypeFullName;

    public FieldInfoImpl() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setFieldTypeFullName(String fieldTypeFullName) {
        this.fieldTypeFullName = fieldTypeFullName;
    }

    @Override
    public String toString() {
        return "FieldInfoImpl{" +
            "name='" + name + '\'' +
            ", fieldTypeFullName='" + fieldTypeFullName + '\'' +
            '}';
    }
}
