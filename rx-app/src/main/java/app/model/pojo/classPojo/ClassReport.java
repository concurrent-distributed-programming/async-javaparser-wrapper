package app.model.pojo.classPojo;

import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;

import java.util.List;

public interface ClassReport {
    String getFullClassName();

    void setFullClassName(String fullClassName);

    List<FieldInfo> getFieldInfo();

    void setFieldInfo(List<FieldInfo> fieldInfo);

    List<MethodInfo> getMethodInfo();

    void setMethodInfo(List<MethodInfo> methodInfo);

    void setSrcFullFilename(String srcFullFilename);

    void setInterface(boolean isInterface);

    boolean isInterface();

    String getSrcFullFileName();
}
