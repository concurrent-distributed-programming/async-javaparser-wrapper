package app.controller.pubsub;

import app.model.AsyncParserModel;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.packagePojo.PackageReport;
import app.model.pojo.packagePojo.PackageReportImpl;
import app.model.visitor_adapters.ClassVisitor;
import app.model.visitor_adapters.FieldVisitor;
import app.model.visitor_adapters.MethodVisitor;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class AnalyzerParserImpl implements AnalyzerParser {
    private final Logger logger = LoggerFactory.getLogger(AnalyzerParserImpl.class);
    private final AsyncParserModel model;

    public AnalyzerParserImpl(AsyncParserModel model) {
        this.model = model;
    }

    @Override
    public List<ClassReport> parseSource(File file) throws FileNotFoundException {
        CompilationUnit cu = getCompilationUnit(file);
        ClassVisitor classVisitor = new ClassVisitor();
        List<ClassReport> classReports = new ArrayList<>();
        classVisitor.visit(cu, classReports);
        classReports.forEach(c -> c.setSrcFullFilename(file.getAbsolutePath()));
        classReports.forEach(c -> logger.warn(c.getFullClassName()));
        return classReports;
    }

    @Override
    public ClassReport parseClassReport(ClassReport report) throws FileNotFoundException {
        File file = new File(report.getSrcFullFileName());
        CompilationUnit cu = getCompilationUnit(file);
        FieldVisitor fieldVisitor = new FieldVisitor();
        MethodVisitor methodVisitor = new MethodVisitor();

        List<FieldInfo> fieldInfo = new ArrayList<>();
        fieldVisitor.visit(cu, fieldInfo);

        List<MethodInfo> methodInfo = new ArrayList<>();
        methodVisitor.visit(cu, methodInfo);

        report.setFieldInfo(fieldInfo);
        report.setMethodInfo(methodInfo);
        return report;
    }

    @Override
    public ObservableSource<PackageReport> parsePackageFile(File packageFile) {
        String projectRootPath = model.getProjectRootPath();
        File projectRootFile = new File(projectRootPath);
        URI packagePathURI = packageFile.toURI();
        URI projectPathURI = projectRootFile.toURI();
        URI relativePathURI = projectPathURI.relativize(packagePathURI);
        String relativePath = relativePathURI.getPath().replace(File.separatorChar, '.');
        String path = String.join(".", projectRootFile.getName(), relativePath);
        path = path.substring(0, path.length() - 1);

        PackageReport packageReport = new PackageReportImpl();
        packageReport.setFullPackageName(path);
        return Observable.just(packageReport);
    }

    private CompilationUnit getCompilationUnit(File file) throws FileNotFoundException {
        return StaticJavaParser.parse(file);
    }
}
