package app.controller.pubsub;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.reactivex.rxjava3.core.ObservableSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface AnalyzerParser {
    List<ClassReport> parseSource(File file) throws FileNotFoundException;

    ClassReport parseClassReport(ClassReport report) throws FileNotFoundException;

    ObservableSource<PackageReport> parsePackageFile(File packageFile);
}
