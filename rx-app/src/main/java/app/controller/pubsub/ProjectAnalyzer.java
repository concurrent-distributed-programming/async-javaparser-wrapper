package app.controller.pubsub;

import app.model.FileTreeNode;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;

public interface ProjectAnalyzer {
    void analyzeProject(String projectRootPath, int maxDepth, Consumer<FileTreeNode> onNext, Action onDispose);

    void stopAnalysis();
}
