package app.controller.pubsub;

import app.model.FileTreeNode;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;

import java.util.List;

public interface NodeCreation {
    List<FileTreeNode> parseClass(ClassReport report);

    List<FileTreeNode> parseInterface(ClassReport report);

    FileTreeNode parsePackage(PackageReport report);
}
