package app.controller.pubsub;

import app.model.FileTreeNode;
import app.model.FileTreeNodeIcon;
import app.model.FileTreeNodeImpl;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.packagePojo.PackageReport;

import java.util.ArrayList;
import java.util.List;

public class NodeCreationImpl implements NodeCreation {
    @Override
    public List<FileTreeNode> parseClass(ClassReport report) {
        List<FileTreeNode> nodes = new ArrayList<>();
        nodes.add(
            getNode(
                report,
                FileTreeNodeIcon.CLASS,
                report.getFullClassName().replaceFirst("[.][^.]+$", "")
            )
        );
        report.getFieldInfo().forEach(
            fieldInfo -> nodes.add(getNode(fieldInfo, report.getFullClassName()))
        );
        report.getMethodInfo().forEach(
            methodInfo -> nodes.add(getNode(methodInfo, report.getFullClassName()))
        );
        return nodes;
    }

    @Override
    public List<FileTreeNode> parseInterface(ClassReport report) {
        List<FileTreeNode> nodes = new ArrayList<>();
        nodes.add(
            getNode(
                report,
                FileTreeNodeIcon.INTERFACE,
                report.getFullClassName().replaceFirst("[.][^.]+$", "")
            )
        );
        report.getMethodInfo().forEach(
            methodInfo -> nodes.add(getNode(methodInfo, report.getFullClassName()))
        );
        return nodes;
    }

    @Override
    public FileTreeNode parsePackage(PackageReport report) {
        return new FileTreeNodeImpl(
            report.getFullPackageName(),
            FileTreeNodeIcon.PACKAGE,
            report.getFullPackageName().replaceFirst("[.][^.]+$", "")
        );
    }

    private FileTreeNode getNode(ClassReport report, FileTreeNodeIcon icon, String parentPath) {
        return new FileTreeNodeImpl(report.getFullClassName(), icon, parentPath);
    }

    private FileTreeNode getNode(MethodInfo methodInfo, String parentPath) {
        return new FileTreeNodeImpl(methodInfo.getName(), FileTreeNodeIcon.METHOD, parentPath);
    }

    private FileTreeNode getNode(FieldInfo fieldInfo, String parentPath) {
        return new FileTreeNodeImpl(fieldInfo.getName(), FileTreeNodeIcon.FIELD, parentPath);
    }
}
