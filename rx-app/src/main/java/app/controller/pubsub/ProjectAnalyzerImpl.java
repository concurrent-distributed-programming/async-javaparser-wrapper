package app.controller.pubsub;

import app.model.AsyncParserModel;
import app.model.FileTreeNode;
import app.model.pojo.classPojo.ClassReport;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class ProjectAnalyzerImpl implements ProjectAnalyzer {
    private final Logger logger = LoggerFactory.getLogger(ProjectAnalyzerImpl.class);
    private final AnalyzerParser parser;

    private final NodeCreation analyzerSt;

    private Disposable disposable;

    public ProjectAnalyzerImpl(AsyncParserModel model) {
        parser = new AnalyzerParserImpl(model);
        analyzerSt = new NodeCreationImpl();
        disposable = Disposable.empty();
    }

    private ObservableSource<FileTreeNode> parseDirectory(File file) {
        return Observable.just(file)
            .subscribeOn(Schedulers.computation())
            .flatMap(parser::parsePackageFile)
            .map(analyzerSt::parsePackage);
    }

    private ObservableSource<FileTreeNode> parseFile(File file) {
        return Observable.just(file)
            .subscribeOn(Schedulers.computation())
            .filter(this::isJavaSrc)
            .map(parser::parseSource)
            .flatMapIterable(r -> r)
            .map(parser::parseClassReport)
            .map(r -> isInterface(r) ? analyzerSt.parseInterface(r) : analyzerSt.parseClass(r))
            .flatMapIterable(f -> f);
    }

    private Stream<Path> getFileStream(String projectRootPath, int maxDepth) throws IOException {
        return Files.walk(Path.of(projectRootPath), maxDepth);
    }

    @Override
    public void analyzeProject(String projectRootPath, int maxDepth, Consumer<FileTreeNode> onNext, Action onDispose) {
        Observable<FileTreeNode> ob = Observable.empty();
        try {
            ob = Observable.fromStream(getFileStream(projectRootPath, maxDepth))
                .skip(1) // exclude the root node
                .map(Path::toFile)
                .concatMap(p -> p.isDirectory() ? parseDirectory(p) : parseFile(p))
                .doOnDispose(onDispose)
                .doOnError((t) -> logger.error(t.getMessage()));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        disposable = ob.subscribe(onNext);
    }

    @Override
    public void stopAnalysis() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        } else {
            logger.warn("Project Analyzer Observable already disposed");
        }
    }

    private boolean isInterface(ClassReport report) {
        return report.isInterface();
    }

    private boolean isJavaSrc(File file) {
        String e = file.getName();
        return e.substring(e.lastIndexOf(".") + 1).equals("java");
    }
}
