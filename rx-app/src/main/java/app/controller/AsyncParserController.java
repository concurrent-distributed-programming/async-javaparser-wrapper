package app.controller;

import app.model.FileTreeNode;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;

public interface AsyncParserController {
    void start(Consumer<FileTreeNode> onNext, Action onDispose);

    void stop();

    void setProjectRoot(String projectRootPath);
}
