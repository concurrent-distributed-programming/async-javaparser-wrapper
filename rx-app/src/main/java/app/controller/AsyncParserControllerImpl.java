package app.controller;

import app.controller.pubsub.ProjectAnalyzer;
import app.controller.pubsub.ProjectAnalyzerImpl;
import app.model.AsyncParserModel;
import app.model.FileTreeNode;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;

public class AsyncParserControllerImpl implements AsyncParserController {
    private final AsyncParserModel model;
    private final ProjectAnalyzer pb;

    public AsyncParserControllerImpl(AsyncParserModel model) {
        this.model = model;
        pb = new ProjectAnalyzerImpl(model);
    }

    @Override
    public void start(Consumer<FileTreeNode> onNext, Action onDispose) {
        pb.analyzeProject(model.getProjectRootPath(), model.getMaxDepth(), onNext, onDispose);
    }

    @Override
    public void stop() {
        pb.stopAnalysis();
    }

    @Override
    public void setProjectRoot(String projectRootPath) {
        model.setProjectRootPath(projectRootPath);
    }

}
