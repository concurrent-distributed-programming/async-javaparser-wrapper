package app;

import app.controller.AsyncParserController;
import app.controller.AsyncParserControllerImpl;
import app.model.AsyncParserModel;
import app.model.AsyncParserModelImpl;
import app.view.AsyncParserView;
import app.view.AsyncParserViewImpl;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncJavaParserLauncher {
    private static final int WIDTH = 820;
    private static final int HEIGHT = 1020;
    private static final Logger logger = LoggerFactory.getLogger(AsyncJavaParserLauncher.class);

    public static void main(String... argv) {
        Args args = new Args();
        JCommander jct = JCommander.newBuilder()
            .addObject(args)
            .build();
        jct.parse(argv);
        if (args.help) {
            jct.usage();
            System.exit(0);
        }

        boolean guiEnabled = args.guiEnabled;
        String projectRootPath = args.path;
        int maxDepth = 5;

        AsyncParserModel model = new AsyncParserModelImpl(projectRootPath);
        AsyncParserController controller = new AsyncParserControllerImpl(model);
        model.setMaxDepth(maxDepth);

        if (guiEnabled) {
            logger.info("starting with GUI");
            AsyncParserView view = new AsyncParserViewImpl(WIDTH, HEIGHT);
            view.createGUI();
            view.display();
            view.addListener(controller);
        } else {
            logger.info("starting without GUI");
            controller.start(v -> logger.info(v.getName()), () -> logger.info("observable disposed successfully"));
        }
    }

    public static class Args {
        @Parameter(names = "--path", description = "Path of the project to analyze")
        private String path = ".";

        @Parameter(names = "--guiEnabled", description = "GUI or command line app")
        private boolean guiEnabled = false;

        @Parameter(names = "--help", help = true, description = "Show help and exit")
        private boolean help = false;
    }
}
