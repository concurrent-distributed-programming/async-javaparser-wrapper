package app.view;

import app.controller.AsyncParserController;
import app.model.FileTreeNode;

public interface AsyncParserView {
    void createGUI();

    void addListener(AsyncParserController controller);

    void display();

    void changeState(final String s);

    void addNode(FileTreeNode interfaceNode);
}
