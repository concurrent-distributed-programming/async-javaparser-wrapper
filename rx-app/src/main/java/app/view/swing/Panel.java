package app.view.swing;

import app.model.FileTreeNode;

public interface Panel {
    void addNode(FileTreeNode node);

    void setProjectName(String projectName);
}
