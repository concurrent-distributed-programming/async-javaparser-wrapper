package app.view.swing.jTree;

import app.model.FileTreeNode;

public interface JTreeBuilding {
    void addLeaf(FileTreeNode node);
}
