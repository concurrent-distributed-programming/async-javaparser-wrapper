package app.view.swing.jTree;

import app.model.FileTreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.Enumeration;
import java.util.Optional;

public class JTreeBuildingImpl implements JTreeBuilding {
    private final Logger logger = LoggerFactory.getLogger(JTreeBuildingImpl.class);
    private final DefaultTreeModel treeModel;

    public JTreeBuildingImpl(DefaultTreeModel treeModel) {
        this.treeModel = treeModel;
    }

//    @Override
//    public void addInterfaceLeaf(ClassReport interfaceReport) {
//        Optional<DefaultMutableTreeNode> parent = jTreeBuildingStrategy.findOrCreateNode(
//            interfaceReport,
//            FileTreeNodeIcon.INTERFACE
//        );
//
//        if (parent.isPresent()) {
//            List<MethodInfo> methods = interfaceReport.getMethodInfo();
//            jTreeBuildingStrategy.addMethodLeafs(methods, parent.get());
//        } else {
//            logger.warn("Got nothing from: {}", interfaceReport.getFullClassName());
//        }
//    }
//
//    @Override
//    public void addClassLeaf(ClassReport classReport) {
//        Optional<DefaultMutableTreeNode> parent = jTreeBuildingStrategy.findOrCreateNode(
//            classReport,
//            FileTreeNodeIcon.CLASS
//        );
//
//        if (parent.isPresent()) {
//            DefaultMutableTreeNode parentNode = parent.get();
//
//            List<FieldInfo> fields = classReport.getFieldInfo();
//            jTreeBuildingStrategy.addFieldLeafs(fields, parentNode);
//
//            List<MethodInfo> methods = classReport.getMethodInfo();
//            jTreeBuildingStrategy.addMethodLeafs(methods, parentNode);
//        } else {
//            logger.warn("Got nothing from: {}", classReport.getFullClassName());
//        }
//    }
//
//    @Override
//    public void addPackageLeaf(PackageReport packageReport) {
//        jTreeBuildingStrategy.createPackage(packageReport);
//    }

    @Override
    public void addLeaf(FileTreeNode node) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
        createNodeFromString(root, node);
    }

    private Optional<TreePath> findPath(DefaultMutableTreeNode root, String path) {
        Enumeration<TreeNode> e = root.depthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            FileTreeNode report = (FileTreeNode) node.getUserObject();
            if (report.getName().equalsIgnoreCase(path)) {
                return Optional.of(new TreePath(node.getPath()));
            }
        }
        return Optional.empty();
    }

    private void createNodeFromString(DefaultMutableTreeNode root, FileTreeNode fileTreeNode) {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileTreeNode);
        String parentPathString = fileTreeNode.getParentPath();
        Optional<TreePath> parentPath = findPath(root, parentPathString);

        if (parentPath.isPresent()) {
            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) parentPath.get().getLastPathComponent();
            treeModel.insertNodeInto(node, parent, parent.getChildCount());
            logger.info("Inserted {} with parent path {}", fileTreeNode.getName(), parentPathString);
        } else {
            logger.warn("Got nothing from {} with parent path {}", fileTreeNode.getName(), parentPathString);
        }
    }
}
