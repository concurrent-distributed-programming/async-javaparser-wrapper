package app.view.swing;

import app.controller.AsyncParserController;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.Optional;

public class FrameStrategyImpl implements FrameStrategy {

    private boolean alreadyChosen = false;
    private boolean alreadyStopped = false;
    private final Panel panel;
    private final Frame frame;
    private final Component parent;

    public FrameStrategyImpl(Frame frame, Panel panel, Component parent) {
        this.parent = parent;
        this.frame = frame;
        this.panel = panel;
    }

    @Override
    public void choosePath(List<AsyncParserController> listeners) {
        if (alreadyChosen) {
            showErrorDialog("The Async Parser must be restarted to choose a new path");
        } else {
            alreadyChosen = true;
            Optional<File> optFile = showProjectDirectoryChooser();
            if (optFile.isPresent()) {
                File file = optFile.get();
                notifyChosenPath(file.getAbsolutePath(), listeners);
                panel.setProjectName(file.getName());
            }
        }
    }

    @Override
    public void stopApp(List<AsyncParserController> listeners) {
        if (alreadyChosen) {
            notifyStopped(listeners);
        } else {
            showErrorDialog("The Async Parser must have been started first");
        }
    }

    private Optional<File> showProjectDirectoryChooser() {
        JFileChooser j = new JFileChooser(
            FileSystemView.getFileSystemView().getHomeDirectory());
        j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int r = j.showOpenDialog(parent);
        if (r == JFileChooser.APPROVE_OPTION) {
            return Optional.of(j.getSelectedFile());
        } else {
            return Optional.empty();
        }
    }

    private void showErrorDialog(String msg) {
        JOptionPane.showMessageDialog(
            parent,
            msg,
            "Async Java Parser Error",
            JOptionPane.ERROR_MESSAGE
        );
    }

    private void notifyChosenPath(String path, List<AsyncParserController> listeners) {
        for (AsyncParserController l : listeners) {
            l.setProjectRoot(path);
            l.start(panel::addNode, () -> frame.updateText("Analysis interrupted"));
        }
    }

    private void notifyStopped(List<AsyncParserController> listeners) {
        if (alreadyStopped) {
            showErrorDialog("The Async Parser has already been stopped");
        } else {
            alreadyStopped = true;
            for (AsyncParserController l : listeners) {
                l.stop();
            }
        }
    }
}
