package app.view.swing;

import app.controller.AsyncParserController;
import app.model.FileTreeNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class VisualiserFrame extends JFrame implements ActionListener, Frame {
    private final static String TITLE = "Async Java Parser Viewer";
    private final VisualiserPanel panel;
    private final JTextField state;
    private final List<AsyncParserController> listeners;
    private final FrameStrategy frameStrategy;

    public VisualiserFrame(int w, int h) {
        super(TITLE);
        setSize(w, h);
        setResizable(false);

        JButton projectSelectionButton = new JButton("select project");
        JButton stopButton = new JButton("stop");
        JPanel controlPanel = new JPanel();
        projectSelectionButton.setFocusable(false);
        stopButton.setFocusable(false);
        controlPanel.add(projectSelectionButton);
        controlPanel.add(stopButton);

        listeners = new ArrayList<>();
        panel = new VisualiserPanel();
        frameStrategy = new FrameStrategyImpl(this, panel, this);

        JPanel infoPanel = new JPanel();
        state = new JTextField(20);
        state.setText("Idle");
        state.setEditable(false);
        infoPanel.add(new JLabel("State"));
        infoPanel.add(state);
        JPanel cp = new JPanel();
        LayoutManager layout = new BorderLayout();
        cp.setLayout(layout);
        cp.add(BorderLayout.NORTH, controlPanel);
        cp.add(BorderLayout.CENTER, panel);
        cp.add(BorderLayout.SOUTH, infoPanel);
        setContentPane(cp);

        projectSelectionButton.addActionListener(this);
        stopButton.addActionListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void addListener(AsyncParserController controller) {
        listeners.add(controller);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        String cmd = ev.getActionCommand();
        switch (cmd) {
            case "select project":
                frameStrategy.choosePath(listeners);
                break;
            case "stop":
                frameStrategy.stopApp(listeners);
                break;
            default:
        }
    }

    @Override
    public void addNode(FileTreeNode node) {
        SwingUtilities.invokeLater(() -> panel.addNode(node));
    }

    @Override
    public void updateText(String s) {
        SwingUtilities.invokeLater(() -> state.setText(s));
    }

    @Override
    public void display() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }
}
