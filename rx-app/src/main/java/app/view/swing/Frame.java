package app.view.swing;

import app.controller.AsyncParserController;
import app.model.FileTreeNode;

public interface Frame {
    void updateText(final String s);

    void addListener(AsyncParserController controller);

    void addNode(FileTreeNode node);

    void display();
}
