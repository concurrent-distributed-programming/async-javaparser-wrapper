package app.view.swing;

import app.controller.AsyncParserController;

import java.util.List;

public interface FrameStrategy {
    void choosePath(List<AsyncParserController> listeners);

    void stopApp(List<AsyncParserController> listeners);
}
