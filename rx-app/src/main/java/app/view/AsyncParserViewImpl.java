package app.view;

import app.controller.AsyncParserController;
import app.model.FileTreeNode;
import app.view.swing.VisualiserFrame;

public class AsyncParserViewImpl implements AsyncParserView {

    private final int w;
    private final int h;
    private VisualiserFrame frame;

    public AsyncParserViewImpl(int w, int h) {
        this.w = w;
        this.h = h;
    }

    @Override
    public void createGUI() {
        frame = new VisualiserFrame(w, h);
    }

    @Override
    public void addListener(AsyncParserController controller) {
        frame.addListener(controller);
    }

    @Override
    public void display() {
        frame.display();
    }

    @Override
    public void changeState(String s) {
        frame.updateText(s);
    }

    @Override
    public void addNode(FileTreeNode node) {
        frame.addNode(node);
    }
}
