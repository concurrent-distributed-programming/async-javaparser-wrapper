package app.model;

import javax.swing.*;

public class FileTreeNodeImpl implements FileTreeNode {
    private final FileTreeNodeIcon node;
    private String name;

    public FileTreeNodeImpl(String name, FileTreeNodeIcon node) {
        this.name = name;
        this.node = node;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ImageIcon getIconPath() {
        return node.getImageIcon(node);
    }

    @Override
    public String toString() {
        return "ProjectElement{" +
            "name=" + name +
            ", icon=" + node.toString() +
            '}';
    }
}
