package app.model;

import javax.swing.*;

public interface FileTreeNode {
    String getName();

    void setName(String name);

    ImageIcon getIconPath();
}
