package app.model;

public interface AsyncParserModel {
    String getProjectRootPath();

    void setProjectRootPath(String projectRootPath);
}
