package app.model.visitor_adapters;

import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.methodPojo.MethodInfoImpl;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

public class MethodVisitor extends VoidVisitorAdapter<List<MethodInfo>> {
    public void visit(MethodDeclaration md, List<MethodInfo> collector) {
        super.visit(md, collector);
        if (md.getBegin().isPresent() && md.getEnd().isPresent()) {
            MethodInfo methodInfo = new MethodInfoImpl();
            methodInfo.setName(md.getNameAsString());
            methodInfo.setSrcBeginLine(md.getBegin().get().line);
            methodInfo.setEndBeginLine(md.getEnd().get().line);
            collector.add(methodInfo);
        }
    }
}
