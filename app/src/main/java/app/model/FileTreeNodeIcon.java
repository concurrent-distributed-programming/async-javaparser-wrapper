package app.model;

import javax.swing.*;
import java.util.Objects;

public enum FileTreeNodeIcon {
    METHOD("icons/method.png"),
    FIELD("icons/field.png"),
    INTERFACE("icons/interface.png"),
    CLASS("icons/class.png"),
    PACKAGE("icons/package.png");

    private final String iconPath;

    FileTreeNodeIcon(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getIconPath() {
        return iconPath;
    }

    public ImageIcon getImageIcon(FileTreeNodeIcon nodeType) {
        return new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(nodeType.getIconPath())));
    }
}
