package app.model;

public class AsyncParserModelImpl implements AsyncParserModel {
    private String projectRootPath;

    public AsyncParserModelImpl(String projectRootPath) {
        this.projectRootPath = projectRootPath;
    }

//    @Override
//    public Future<ClassReport> getInterfaceReport() {
//        return analyzer.getInterfaceReport(projectRootPath);
//    }
//
//    @Override
//    public Future<ClassReport> getClassReport() {
//        return analyzer.getClassReport(projectRootPath);
//    }
//
//    @Override
//    public Future<PackageReport> getPackageReport() {
//        return analyzer.getPackageReport(projectRootPath);
//    }
//
//    @Override
//    public Future<ProjectReport> getProjectReport() {
//        return analyzer.getProjectReport(projectRootPath);
//    }
//
//    @Override
//    public void analyzeProject() {
//        // TODO: implement this
//        throw new UnsupportedOperationException();
//    }

    @Override
    public String getProjectRootPath() {
        return projectRootPath;
    }

    @Override
    public void setProjectRootPath(String projectRootPath) {
        this.projectRootPath = projectRootPath;
    }
}
