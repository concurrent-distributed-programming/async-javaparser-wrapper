package app.model.pojo.classPojo;

import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;

import java.util.List;

public class ClassReportImpl implements ClassReport {

    private String fullClassName;
    private String srcFullFileName;
    private List<FieldInfo> fieldInfo;
    private List<MethodInfo> methodInfo;

    public ClassReportImpl() {
    }

    @Override
    public String getFullClassName() {
        return fullClassName;
    }

    @Override
    public void setFullClassName(String fullClassName) {
        this.fullClassName = fullClassName;
    }

    @Override
    public List<FieldInfo> getFieldInfo() {
        return fieldInfo;
    }

    @Override
    public void setFieldInfo(List<FieldInfo> fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    @Override
    public List<MethodInfo> getMethodInfo() {
        return methodInfo;
    }

    @Override
    public void setMethodInfo(List<MethodInfo> methodInfo) {
        this.methodInfo = methodInfo;
    }

    @Override
    public void setSrcFullFilename(String srcFullFilename) {
        this.srcFullFileName = srcFullFilename;
    }

    @Override
    public String toString() {
        return "ClassReportImpl{" +
            "fullClassName='" + fullClassName + '\'' +
            ", srcFullFilename='" + srcFullFileName + '\'' +
            ", fieldInfo=" + fieldInfo +
            ", methodInfo=" + methodInfo +
            '}';
    }
}
