package app.model.pojo.packagePojo;

public class PackageReportImpl implements PackageReport {

    private String fullPackageName;

    public PackageReportImpl() {
    }

    @Override
    public String getFullPackageName() {
        return fullPackageName;
    }

    @Override
    public void setFullPackageName(String fullPackageName) {
        this.fullPackageName = fullPackageName;
    }

}
