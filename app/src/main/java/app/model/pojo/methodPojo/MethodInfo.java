package app.model.pojo.methodPojo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = MethodInfoImpl.class)
public interface MethodInfo {
    String getName();

    void setName(String name);

    int getSrcBeginLine();

    void setSrcBeginLine(int srcBeginLine);

    void setEndBeginLine(int endBeginLine);
}
