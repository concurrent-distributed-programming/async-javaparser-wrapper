package app.model.pojo.fieldPojo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = FieldInfoImpl.class)
public interface FieldInfo {
    String getName();

    void setName(String name);

    void setFieldTypeFullName(String fieldTypeFullName);
}
