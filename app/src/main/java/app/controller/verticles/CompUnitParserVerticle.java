package app.controller.verticles;

import app.controller.project_analyzer.ProjectAnalyzer;
import app.controller.project_analyzer.ProjectAnalyzerImpl;
import app.controller.tree_builder.TreeBuilding;
import app.controller.tree_builder.TreeBuildingImpl;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CompUnitParserVerticle extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(CompUnitParserVerticle.class);
    private ProjectAnalyzer projectAnalyzer;
    private TreeBuilding treeBuilding;

    public void start() {
        EventBus eventBus = getVertx().eventBus();
        projectAnalyzer = new ProjectAnalyzerImpl();
        treeBuilding = new TreeBuildingImpl(eventBus);

        eventBus.consumer("source-discovered",
            msg -> {
                String path = msg.body().toString();
                Future<List<ClassReport>> classReportsFuture = projectAnalyzer.getClasses(path);
                classReportsFuture.onSuccess((res) -> {
                    for (ClassReport classReport : res) {
                        Future<ClassReport> futureReport = projectAnalyzer.getClassReport(classReport, path);
                        treeBuilding.sendClassNode(futureReport);
                    }
                });
            }
        );
        eventBus.consumer("package-discovered",
            msg -> {
                String path = msg.body().toString();
                Future<PackageReport> futureReport = projectAnalyzer.getPackageReport(path, config().getString("path"));
                treeBuilding.sendPackageNode(futureReport);
            }
        );
    }
}
