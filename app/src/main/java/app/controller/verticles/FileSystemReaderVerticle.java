package app.controller.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.file.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class FileSystemReaderVerticle extends AbstractVerticle {
    private final Logger logger = LoggerFactory.getLogger(FileSystemReaderVerticle.class);
    private EventBus eventBus;
    private FileSystem fs;

    private void readFileSystem(String path) {
        Future<List<String>> future = fs.readDir(path);
        future.onComplete((AsyncResult<List<String>> res) -> {
            if (res.succeeded()) {
                List<String> packages =
                    res.result()
                        .stream()
                        .map(File::new)
                        .filter(File::isDirectory)
                        .map(File::getAbsolutePath)
                        .collect(Collectors.toList());

                if (!packages.isEmpty()) {
                    packages.forEach(p -> {
                        logger.info("Sent search-request for: {}", p);
                        eventBus.publish("package-discovered", p);
                        eventBus.publish("search-request", p);
                    });
                }

                List<String> sources =
                    res.result()
                        .stream()
                        .filter(e -> e.substring(e.lastIndexOf(".") + 1).equals("java"))
                        .collect(Collectors.toList());

                if (!sources.isEmpty()) {
                    sources.forEach(s -> eventBus.publish("source-discovered", s));
                }
            } else {
                logger.error(res.cause().toString());
            }
        });
    }

    public void start() {
        eventBus = getVertx().eventBus();
        fs = getVertx().fileSystem();
        eventBus.consumer("search-request", msg -> {
            String path = msg.body().toString();
            readFileSystem(path);
            logger.info("Received search-request for: {}", path);
        });
    }
}
