package app.controller;

import app.controller.tree_builder.TreeNodeListener;
import app.controller.tree_builder.TreeNodeListenerImpl;
import app.controller.verticles.CompUnitParserVerticle;
import app.controller.verticles.FileSystemReaderVerticle;
import app.model.AsyncParserModel;
import app.view.AsyncParserView;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AsyncParserControllerImpl implements AsyncParserController {
    private final Logger logger = LoggerFactory.getLogger(AsyncParserControllerImpl.class);
    private final AsyncParserModel model;
    private final List<AsyncParserView> listeners;
    private final TreeNodeListener treeNodeListener;
    private final Handler<AsyncResult<String>> completionHandler;
    private final Vertx vertx;

    public AsyncParserControllerImpl(AsyncParserModel model) {
        vertx = Vertx.vertx();
        this.model = model;
        listeners = new ArrayList<>();
        treeNodeListener = new TreeNodeListenerImpl(vertx.eventBus(), listeners);
        completionHandler = (deployedEvent) -> {
            logger.info("deployed verticle: " + deployedEvent.result());
            vertx.eventBus().consumer("shutdown-request",
                finishedMessage -> vertx.undeploy(deployedEvent.result(),
                    undeployEvent -> logger.info("undeployed verticle: " + deployedEvent.result())));
        };
    }

    @Override
    public void start() {
        String path = model.getProjectRootPath();
        DeploymentOptions options = new DeploymentOptions().setConfig(new JsonObject().put("path", path));

        treeNodeListener.registerListeners();
        vertx.deployVerticle(new CompUnitParserVerticle(), options, completionHandler);
        vertx.deployVerticle(new FileSystemReaderVerticle(), completionHandler);
        vertx.eventBus().publish("search-request", path);

        logger.info("Sent search-request for: {}", path);
    }

    public void stop() {
        EventBus eb = vertx.eventBus();
        eb.publish("shutdown-request", "");
    }

    @Override
    public void setProjectRoot(String projectRootPath) {
        model.setProjectRootPath(projectRootPath);
    }

    @Override
    public void addListener(AsyncParserView view) {
        listeners.add(view);
    }

    @Override
    public void notifyStateChange(String stateDescription) {
        for (AsyncParserView v : listeners) {
            v.changeState(stateDescription);
        }
    }
}
