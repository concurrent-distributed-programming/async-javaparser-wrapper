package app.controller.tree_builder;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.Future;

public interface TreeBuilding {
    void sendClassNode(Future<ClassReport> interfaceReport);

    void sendPackageNode(Future<PackageReport> interfaceReport);
}
