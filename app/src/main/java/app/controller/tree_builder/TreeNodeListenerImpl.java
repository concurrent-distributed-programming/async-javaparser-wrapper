package app.controller.tree_builder;

import app.model.pojo.classPojo.ClassReportImpl;
import app.model.pojo.packagePojo.PackageReportImpl;
import app.view.AsyncParserView;
import com.fasterxml.jackson.core.type.TypeReference;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.jackson.JacksonCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TreeNodeListenerImpl implements TreeNodeListener {
    private final Logger logger = LoggerFactory.getLogger(TreeNodeListenerImpl.class);
    private final EventBus eventBus;
    private final List<AsyncParserView> listeners;

    public TreeNodeListenerImpl(EventBus eventBus, List<AsyncParserView> listeners) {
        this.eventBus = eventBus;
        this.listeners = listeners;
    }

    public void registerListeners() {
        onNewPackage();
        onNewInterface();
        onNewClass();
        onError();
        onShutdown();
    }

    private void onNewPackage() {
        eventBus.consumer("new-package", message -> {
            JsonObject jsonObject = (JsonObject) message.body();
            logger.debug(jsonObject.encodePrettily());
            TypeReference<PackageReportImpl> ref = new TypeReference<>() {
            };
            PackageReportImpl report = JacksonCodec.decodeValue(jsonObject.encode(), ref);
            logger.info("Got: {}", report.getFullPackageName());
            listeners.forEach(l -> l.addPackage(report));
        });
    }

    private void onNewInterface() {
        eventBus.consumer("new-interface", message -> listeners.forEach(
            l -> l.addInterface(parseClassOrInterface(message))));
    }

    private void onNewClass() {
        eventBus.consumer("new-class", message -> listeners.forEach(
            l -> l.addClass(parseClassOrInterface(message))));
    }

    private ClassReportImpl parseClassOrInterface(Message<Object> message) {
        JsonObject jsonObject = (JsonObject) message.body();
        logger.debug(jsonObject.encodePrettily());
        TypeReference<ClassReportImpl> ref = new TypeReference<>() {
        };
        ClassReportImpl report = JacksonCodec.decodeValue(jsonObject.encode(), ref);
        logger.debug("Got: {}", report.getFullClassName());
        return report;
    }

    private void onError() {
        eventBus.consumer("error", msg -> {
            listeners.forEach(l -> l.changeState("Failure"));
            eventBus.publish("shutdown-request", "");
        });
    }

    private void onShutdown() {
        eventBus.consumer("shutdown-request", msg -> listeners.forEach(
            l -> l.changeState("Interrupted")));
    }
}
