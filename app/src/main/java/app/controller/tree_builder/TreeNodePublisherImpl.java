package app.controller.tree_builder;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TreeNodePublisherImpl implements TreeNodePublisher {
    private final Logger logger = LoggerFactory.getLogger(TreeNodePublisherImpl.class);
    private final EventBus eventBus;

    public TreeNodePublisherImpl(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void notifyNewPackage(PackageReport packageReport) {
        JsonObject jsonObject = new JsonObject(Json.encode(packageReport));
        eventBus.publish("new-package", jsonObject);
    }

    @Override
    public void notifyNewClass(ClassReport classReport) {
        JsonObject jsonObject = new JsonObject(Json.encode(classReport));
        eventBus.publish("new-class", jsonObject);
    }

    @Override
    public void notifyNewInterface(ClassReport interfaceReport) {
        JsonObject jsonObject = new JsonObject(Json.encode(interfaceReport));
        eventBus.publish("new-interface", jsonObject);
    }

    @Override
    public void notifyError(String msg) {
        logger.info("error: {}", msg);
        eventBus.publish("error", msg);
    }

}
