package app.controller.tree_builder;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TreeBuildingImpl implements TreeBuilding {
    private final Logger logger = LoggerFactory.getLogger(TreeBuildingImpl.class);
    private final TreeNodePublisher treeNodePublisher;

    public TreeBuildingImpl(EventBus eventBus) {
        treeNodePublisher = new TreeNodePublisherImpl(eventBus);
    }

    @Override
    public void sendClassNode(Future<ClassReport> classReport) {
        classReport.onSuccess((ClassReport res) -> {
            if (res.getFieldInfo().isEmpty()) {
                treeNodePublisher.notifyNewInterface(res);
            } else {
                treeNodePublisher.notifyNewClass(res);
            }
        }).onFailure((Throwable t) -> {
            logger.error(t.getMessage());
            treeNodePublisher.notifyError(t.getMessage());
        });
    }

    @Override
    public void sendPackageNode(Future<PackageReport> packageReport) {
        packageReport.onSuccess(treeNodePublisher::notifyNewPackage).onFailure((Throwable t) -> {
            logger.error(t.getMessage());
            treeNodePublisher.notifyError(t.getMessage());
        });
    }
}
