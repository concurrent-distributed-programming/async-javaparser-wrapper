package app.controller.tree_builder;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;

public interface TreeNodePublisher {
    void notifyNewInterface(ClassReport interfaceReport);

    void notifyNewClass(ClassReport classReport);

    void notifyNewPackage(PackageReport packageReport);

    void notifyError(String msg);
}
