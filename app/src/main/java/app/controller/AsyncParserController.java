package app.controller;

public interface AsyncParserController extends StateObserver {
    void start();

    void stop();

    void setProjectRoot(String projectRootPath);
}
