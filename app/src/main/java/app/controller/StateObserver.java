package app.controller;

import app.view.AsyncParserView;

public interface StateObserver {
    void addListener(AsyncParserView view);

    void notifyStateChange(String stateDescription);
}
