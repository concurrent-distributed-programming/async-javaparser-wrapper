package app.controller.project_analyzer;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.Future;

import java.util.List;

public interface ProjectAnalyzer {

    Future<ClassReport> getClassReport(ClassReport classReport, String srcClassPath);

    Future<PackageReport> getPackageReport(String srcPackagePath, String projectPath);

    Future<List<ClassReport>> getClasses(String srcClassPath);
}
