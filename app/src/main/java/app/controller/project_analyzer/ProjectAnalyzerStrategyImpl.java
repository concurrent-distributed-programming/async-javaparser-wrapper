package app.controller.project_analyzer;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.packagePojo.PackageReport;
import app.model.pojo.packagePojo.PackageReportImpl;
import app.model.visitor_adapters.ClassVisitor;
import app.model.visitor_adapters.FieldVisitor;
import app.model.visitor_adapters.MethodVisitor;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import io.vertx.core.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class ProjectAnalyzerStrategyImpl implements ProjectAnalyzerStrategy {
    private final Logger logger = LoggerFactory.getLogger(ProjectAnalyzerStrategyImpl.class);
    private final FieldVisitor fieldVisitor;
    private final MethodVisitor methodVisitor;
    private final ClassVisitor classVisitor;

    public ProjectAnalyzerStrategyImpl() {
        fieldVisitor = new FieldVisitor();
        methodVisitor = new MethodVisitor();
        classVisitor = new ClassVisitor();
//        packageVisitor = new PackageVisitor();
    }

    private CompilationUnit getCompilationUnit(File file) throws FileNotFoundException {
        return StaticJavaParser.parse(file);
    }

    @Override
    public void computeClassReportAsync(Promise<ClassReport> ps, ClassReport report, String srcClassPath) {
        File file = new File(srcClassPath);
        report.setSrcFullFilename(file.getAbsolutePath());
        List<FieldInfo> fieldInfo = new ArrayList<>();
        List<MethodInfo> methodInfo = new ArrayList<>();

        try {
            CompilationUnit cu = getCompilationUnit(file);
            fieldVisitor.visit(cu, fieldInfo);
            methodVisitor.visit(cu, methodInfo);
            report.setFieldInfo(fieldInfo);
            report.setMethodInfo(methodInfo);
            ps.complete(report);
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
            ps.fail(e);
        }
    }

    @Override
    public void computePackageReportAsync(Promise<PackageReport> ps, String srcPackagePath, String projectPath) {
        logger.info(projectPath);
        logger.info(srcPackagePath);
        File packageFile = new File(srcPackagePath);
        File projectRootFile = new File(projectPath);
        URI packagePathURI = packageFile.toURI();
        URI projectPathURI = projectRootFile.toURI();
        URI relativePathURI = projectPathURI.relativize(packagePathURI);
        String relativePath = relativePathURI.getPath().replace(File.separatorChar, '.');
        String path = String.join(".", projectRootFile.getName(), relativePath);
        path = path.substring(0, path.length() - 1);

        PackageReport report = new PackageReportImpl();
        report.setFullPackageName(path);
        ps.complete(report);
    }

    @Override
    public void getClasses(Promise<List<ClassReport>> ps, String srcClassPath) {
        File file = new File(srcClassPath);
        List<ClassReport> classReports = new ArrayList<>();

        try {
            CompilationUnit cu = getCompilationUnit(file);
            classVisitor.visit(cu, classReports);
            ps.complete(classReports);
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
            ps.fail(e);
        }
    }

}
