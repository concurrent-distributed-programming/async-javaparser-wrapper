package app.controller.project_analyzer;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.Promise;

import java.util.List;

public interface ProjectAnalyzerStrategy {
    void computeClassReportAsync(Promise<ClassReport> ps, ClassReport report, String srcClassPath);

    void computePackageReportAsync(Promise<PackageReport> ps, String srcPackagePath, String projectPath);

    void getClasses(Promise<List<ClassReport>> ps, String srcClassPath);
}
