package app.controller.project_analyzer;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import io.vertx.core.Future;
import io.vertx.core.Promise;

import java.util.List;

public class ProjectAnalyzerImpl implements ProjectAnalyzer {

    private final ProjectAnalyzerStrategy analyzerStrategy;

    public ProjectAnalyzerImpl() {
        this.analyzerStrategy = new ProjectAnalyzerStrategyImpl();
    }

    @Override
    public Future<ClassReport> getClassReport(ClassReport classReport, String srcClassPath) {
        Promise<ClassReport> ps = Promise.promise();
        analyzerStrategy.computeClassReportAsync(ps, classReport, srcClassPath);
        return ps.future();
    }

    @Override
    public Future<PackageReport> getPackageReport(String srcPackagePath, String projectPath) {
        Promise<PackageReport> ps = Promise.promise();
        analyzerStrategy.computePackageReportAsync(ps, srcPackagePath, projectPath);
        return ps.future();
    }

    @Override
    public Future<List<ClassReport>> getClasses(String srcClassPath) {
        Promise<List<ClassReport>> ps = Promise.promise();
        analyzerStrategy.getClasses(ps, srcClassPath);
        return ps.future();
    }
}
