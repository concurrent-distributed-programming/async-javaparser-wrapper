package app.view.swing;

import app.controller.AsyncParserController;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class VisualiserFrame extends JFrame implements ActionListener, Frame {
    private final Logger logger = LoggerFactory.getLogger(VisualiserFrame.class);
    private final static String TITLE = "Async Java Parser Viewer";
    private final VisualiserPanel panel;
    private final JTextField state;
    private final List<AsyncParserController> listeners;
    private String projectName;
    private boolean pathChosen = false;
    private boolean started = false;

    public VisualiserFrame(int w, int h) {
        super(TITLE);
        setSize(w, h);
        setResizable(false);
        listeners = new ArrayList<>();

        JButton projectSelectionButton = new JButton("select project");
        JButton startButton = new JButton("start");
        JButton stopButton = new JButton("stop");
        JPanel controlPanel = new JPanel();
        projectSelectionButton.setFocusable(false);
        startButton.setFocusable(false);
        stopButton.setFocusable(false);
        controlPanel.add(projectSelectionButton);
        controlPanel.add(startButton);
        controlPanel.add(stopButton);

        panel = new VisualiserPanel();

        JPanel infoPanel = new JPanel();
        state = new JTextField(20);
        state.setText("Idle");
        state.setEditable(false);
        infoPanel.add(new JLabel("State"));
        infoPanel.add(state);
        JPanel cp = new JPanel();
        LayoutManager layout = new BorderLayout();
        cp.setLayout(layout);
        cp.add(BorderLayout.NORTH, controlPanel);
        cp.add(BorderLayout.CENTER, panel);
        cp.add(BorderLayout.SOUTH, infoPanel);
        setContentPane(cp);

        projectSelectionButton.addActionListener(this);
        startButton.addActionListener(this);
        stopButton.addActionListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void updateText(String s) {
        SwingUtilities.invokeLater(() -> state.setText(s));
    }

    @Override
    public void addListener(AsyncParserController controller) {
        listeners.add(controller);
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        String cmd = ev.getActionCommand();
        switch (cmd) {
            case "start":
                notifyStarted();
                break;
            case "stop":
                notifyStopped();
                break;
            case "select project":
                Optional<File> optFile = showProjectDirectoryChooser();
                if (optFile.isPresent()) {
                    File file = optFile.get();
                    notifyChosenPath(file.getAbsolutePath());
                    panel.setProjectName(file.getName());
                }
                break;
            default:
        }
    }

    private Optional<File> showProjectDirectoryChooser() {
        JFileChooser j = new JFileChooser(
            FileSystemView.getFileSystemView().getHomeDirectory());
        j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int r = j.showOpenDialog(this);
        if (r == JFileChooser.APPROVE_OPTION) {
            return Optional.of(j.getSelectedFile());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void notifyChosenPath(String path) {
        if (!pathChosen) {
            pathChosen = true;
            for (AsyncParserController l : listeners) {
                l.setProjectRoot(path);
            }
        } else {
            showErrorDialog("A path has already been chosen");
        }
    }

    @Override
    public void notifyStarted() {
        if (pathChosen) {
            started = true;
            for (AsyncParserController l : listeners) {
                l.start();
            }
        } else {
            showErrorDialog("A path must first be chosen");
        }
    }

    private void showErrorDialog(String msg) {
        JOptionPane.showMessageDialog(
            this,
            msg,
            "Async Java Parser Error",
            JOptionPane.ERROR_MESSAGE
        );
    }

    @Override
    public void notifyStopped() {
        if (started) {
            for (AsyncParserController l : listeners) {
                l.stop();
            }
        } else {
            showErrorDialog("The Async Parser must have been started first");
        }
    }

    @Override
    public void addInterface(ClassReport interfaceReport) {
        SwingUtilities.invokeLater(() -> panel.addInterface(interfaceReport));
    }

    @Override
    public void addClass(ClassReport classReport) {
        SwingUtilities.invokeLater(() -> panel.addClass(classReport));
    }

    @Override
    public void addPackage(PackageReport packageReport) {
        SwingUtilities.invokeLater(() -> panel.addPackage(packageReport));
    }

    @Override
    public void display() {
        SwingUtilities.invokeLater(() -> this.setVisible(true));
    }
}
