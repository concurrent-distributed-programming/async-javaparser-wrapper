package app.view.swing.jTree;

import app.model.FileTreeNodeIcon;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.packagePojo.PackageReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.List;
import java.util.Optional;

public class JTreeBuildingImpl implements JTreeBuilding {
    private final Logger logger = LoggerFactory.getLogger(JTreeBuildingImpl.class);
    private final JTreeBuildingStrategy jTreeBuildingStrategy;

    public JTreeBuildingImpl(DefaultTreeModel treeModel) {
        jTreeBuildingStrategy = new JTreeBuildingStrategyImpl(treeModel);
    }

    @Override
    public void addInterfaceLeaf(ClassReport interfaceReport) {
        Optional<DefaultMutableTreeNode> parent = jTreeBuildingStrategy.findOrCreateNode(
            interfaceReport,
            FileTreeNodeIcon.INTERFACE
        );

        if (parent.isPresent()) {
            List<MethodInfo> methods = interfaceReport.getMethodInfo();
            jTreeBuildingStrategy.addMethodLeafs(methods, parent.get());
        } else {
            logger.warn("Got nothing from: {}", interfaceReport.getFullClassName());
        }
    }

    @Override
    public void addClassLeaf(ClassReport classReport) {
        Optional<DefaultMutableTreeNode> parent = jTreeBuildingStrategy.findOrCreateNode(
            classReport,
            FileTreeNodeIcon.CLASS
        );

        if (parent.isPresent()) {
            DefaultMutableTreeNode parentNode = parent.get();

            List<FieldInfo> fields = classReport.getFieldInfo();
            jTreeBuildingStrategy.addFieldLeafs(fields, parentNode);

            List<MethodInfo> methods = classReport.getMethodInfo();
            jTreeBuildingStrategy.addMethodLeafs(methods, parentNode);
        } else {
            logger.warn("Got nothing from: {}", classReport.getFullClassName());
        }
    }

    @Override
    public void addPackageLeaf(PackageReport packageReport) {
        jTreeBuildingStrategy.createPackage(packageReport);
    }
}
//        FileTreeNode packageReportModel = new FileTreeNodeImpl(packageReport.getFullPackageName(), TreeNodeIcon.PACKAGE);
//        DefaultMutableTreeNode packageReportNode = new DefaultMutableTreeNode(packageReportModel);
//
//        List<ClassReport> classes = packageReport.getClassReports();
//        jTreeBuildingStrategy.addClassLeafs(classes);
//        List<PackageReport> packages = projectReport.getAllPackages();
//        addPackageLeafs(packages, parent);
//        TreeNode packageReportModel = new TreeNodeImpl(projectReport.get(), TreeNodeIcon.PACKAGE);
//        DefaultMutableTreeNode packageReportNode = new DefaultMutableTreeNode(packageReportModel);
//        /*
//         * Add package node
//         */
//        TreeNode packageReportModel = new TreeNodeImpl(packageReport.getFullPackageName(), TreeNodeIcon.PACKAGE);
//        DefaultMutableTreeNode packageReportNode = new DefaultMutableTreeNode(packageReportModel);
//        treeModel.insertNodeInto(packageReportNode, parent, parent.getChildCount());
//    @Override
//    public void updateFileTree() {
//        List<TreeNodeImpl> leafs = new ArrayList<>();
//        leafs.add(new TreeNodeImpl("Class", TreeNodeIcon.CLASS));
//        leafs.add(new TreeNodeImpl("Field", TreeNodeIcon.FIELD));
//        leafs.add(new TreeNodeImpl("Interface", TreeNodeIcon.INTERFACE));
//        leafs.add(new TreeNodeImpl("Method", TreeNodeIcon.METHOD));
//        leafs.add(new TreeNodeImpl("Package", TreeNodeIcon.PACKAGE));
//
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode(new TreeNodeImpl("Package", TreeNodeIcon.PACKAGE));
//        DefaultTreeModel treeModel = new DefaultTreeModel(addLeafsToNode(root, leafs));
//        fileTree.setModel(treeModel);
//    }
//    private DefaultMutableTreeNode addLeafsToNode(DefaultMutableTreeNode root, List<TreeNodeImpl> treeNodes) {
//        for (TreeNodeImpl treeNode : treeNodes) {
//            DefaultMutableTreeNode node = new DefaultMutableTreeNode(treeNode);
//            root.add(node);
//        }
//        return root;
//    }
//        root = (DefaultMutableTreeNode) fileTree.getModel().getRoot();
//        treeModel.setRoot(projectReportNode);
