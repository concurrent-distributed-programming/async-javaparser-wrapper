package app.view.swing.jTree;

import app.model.FileTreeNodeImpl;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

public class TreeNodeCellRenderer extends DefaultTreeCellRenderer {

    public TreeNodeCellRenderer() {
    }

    public Component getTreeCellRendererComponent(
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus) {

//        Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
//        if (userObject instanceof TreeNode node) {
//            label.setIcon(node.getImageIcon(node));
//            label.setText(value.toString());
//        } else {
//            label.setIcon(null);
//            label.setText(value.toString());
//        }
//        return label;
        Component component = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        if (node.getUserObject() instanceof FileTreeNodeImpl) {
            FileTreeNodeImpl category = (FileTreeNodeImpl) node.getUserObject();
            ImageIcon imageIcon = category.getIconPath();
            this.setIcon(imageIcon);
            this.setText(category.getName());
        }
        return component;
    }
}
