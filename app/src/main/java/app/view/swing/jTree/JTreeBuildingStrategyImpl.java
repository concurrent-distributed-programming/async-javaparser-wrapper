package app.view.swing.jTree;

import app.model.FileTreeNode;
import app.model.FileTreeNodeIcon;
import app.model.FileTreeNodeImpl;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.packagePojo.PackageReport;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

public class JTreeBuildingStrategyImpl implements JTreeBuildingStrategy {
    private final DefaultTreeModel treeModel;

    public JTreeBuildingStrategyImpl(DefaultTreeModel treeModel) {
        this.treeModel = treeModel;
    }

    private Optional<TreePath> findPath(DefaultMutableTreeNode root, String path) {
        Enumeration<TreeNode> e = root.depthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            FileTreeNode report = (FileTreeNode) node.getUserObject();
            if (report.getName().equalsIgnoreCase(path)) {
                return Optional.of(new TreePath(node.getPath()));
            }
        }
        return Optional.empty();
    }

    private Optional<DefaultMutableTreeNode> createNodeFromString(DefaultMutableTreeNode root, FileTreeNode fileTreeNode) {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileTreeNode);
        String parentPathString = fileTreeNode.getName().replaceFirst("[.][^.]+$", "");
        Optional<TreePath> parentPath = findPath(root, parentPathString);

        if (parentPath.isPresent()) {
            DefaultMutableTreeNode parent = (DefaultMutableTreeNode) parentPath.get().getLastPathComponent();
            treeModel.insertNodeInto(node, parent, parent.getChildCount());
            return Optional.of(node);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<DefaultMutableTreeNode> findOrCreateNode(ClassReport report, FileTreeNodeIcon icon) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
        Optional<TreePath> path = findPath(root, report.getFullClassName());
        if (path.isEmpty()) {
            FileTreeNode fileNode = new FileTreeNodeImpl(report.getFullClassName(), icon);
            return createNodeFromString(root, fileNode);
        }
        return Optional.empty();
    }

    @Override
    public void createPackage(PackageReport report) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
        Optional<TreePath> path = findPath(root, report.getFullPackageName());
        if (path.isEmpty()) {
            FileTreeNode fileNode = new FileTreeNodeImpl(
                report.getFullPackageName(),
                FileTreeNodeIcon.PACKAGE
            );
            createNodeFromString(root, fileNode);
        }
    }

    @Override
    public void addMethodLeafs(List<MethodInfo> methods, DefaultMutableTreeNode parent) {
        methods.forEach(e -> {
            FileTreeNode methodInfoModel = new FileTreeNodeImpl(e.getName(), FileTreeNodeIcon.METHOD);
            DefaultMutableTreeNode methodInfoNode = new DefaultMutableTreeNode(methodInfoModel);
            treeModel.insertNodeInto(methodInfoNode, parent, parent.getChildCount());
        });
    }

    @Override
    public void addFieldLeafs(List<FieldInfo> fields, DefaultMutableTreeNode parent) {
        fields.forEach(e -> {
            FileTreeNode fieldInfoModel = new FileTreeNodeImpl(e.getName(), FileTreeNodeIcon.FIELD);
            DefaultMutableTreeNode fieldInfoNode = new DefaultMutableTreeNode(fieldInfoModel);
            treeModel.insertNodeInto(fieldInfoNode, parent, parent.getChildCount());
        });
    }

}

//    private int childIndex(final DefaultMutableTreeNode node, final String childValue) {
//        Enumeration<DefaultMutableTreeNode> children = node.children();
//        DefaultMutableTreeNode child = null;
//        int index = -1;
//
//        while (children.hasMoreElements() && index < 0) {
//            child = children.nextElement();
//
//            if (child.getUserObject() != null && childValue.equals(child.getUserObject())) {
//                index = node.getIndex(child);
//            }
//        }
//
//        return index;
//    }
//
//    @Override
//    public Optional<DefaultMutableTreeNode> findOrCreateInterface(ClassReport report) {
//        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
//        Optional<TreePath> path = findPath(root, report.getFullClassName());
//        if (path.isEmpty()) {
//            FileTreeNode fileNode = new FileTreeNodeImpl(
//                report.getFullClassName(), FileTreeNodeIcon.INTERFACE
//            );
//            return createNodeFromString(root, fileNode);
//        }
//        return Optional.empty();
//    }

//        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
//        String[] strings = str.split("\\.");
//        DefaultMutableTreeNode node = root;
//
//        for (String s : strings) {
//            // Look for the index of a node at the current level that has a value equal to the current string
//            int index = treeModel.getIndexOfChild(node, s);
//            if (index < 0) {
//                logger.info("Creating node {}", s);
//                DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(s);
//                node.insert(newChild, node.getChildCount());
//                node = newChild;
//            } else {
//                logger.info("Node {} already exists", s);
//                node = (DefaultMutableTreeNode) node.getChildAt(index);
//            }
//        }
//    int index = treeModel.getIndexOfChild(root, s);
//            if (index >= 0) {
//        logger.info("Node {} found", s);
//        return Optional.of((DefaultMutableTreeNode) root.getChildAt(index));
//    }
//    private int childIndex(final DefaultMutableTreeNode node, final String childValue) {
//        Enumeration<DefaultMutableTreeNode> children = node.children();
//        DefaultMutableTreeNode child = null;
//        int index = -1;
//
//        while (children.hasMoreElements() && index < 0) {
//            child = children.nextElement();
//
//            if (child.getUserObject() != null && childValue.equals(child.getUserObject())) {
//                index = node.getIndex(child);
//            }
//        }
//
//        return index;
//    }
