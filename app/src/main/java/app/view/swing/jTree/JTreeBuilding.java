package app.view.swing.jTree;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;

public interface JTreeBuilding {
    void addInterfaceLeaf(ClassReport interfaceReport);

    void addClassLeaf(ClassReport classReport);

    void addPackageLeaf(PackageReport packageReport);
}
