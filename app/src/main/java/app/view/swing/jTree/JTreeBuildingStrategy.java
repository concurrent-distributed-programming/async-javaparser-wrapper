package app.view.swing.jTree;

import app.model.FileTreeNodeIcon;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.fieldPojo.FieldInfo;
import app.model.pojo.methodPojo.MethodInfo;
import app.model.pojo.packagePojo.PackageReport;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;
import java.util.Optional;

public interface JTreeBuildingStrategy {
    Optional<DefaultMutableTreeNode> findOrCreateNode(ClassReport report, FileTreeNodeIcon icon);

    void createPackage(PackageReport report);

    void addMethodLeafs(List<MethodInfo> methods, DefaultMutableTreeNode parent);

    void addFieldLeafs(List<FieldInfo> fields, DefaultMutableTreeNode parent);
}
