package app.view.swing;

import app.controller.AsyncParserController;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;

public interface Frame {
    void updateText(final String s);

    void addListener(AsyncParserController controller);

    void notifyChosenPath(String path);

    void notifyStarted();

    void notifyStopped();

    void addInterface(ClassReport interfaceReport);

    void addClass(ClassReport classReport);

    void addPackage(PackageReport packageReport);

    void display();
}
