package app.view.swing;

import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;

public interface Panel {
    void addInterface(ClassReport interfaceReport);

    void addClass(ClassReport classReport);

    void addPackage(PackageReport packageReport);

    void setProjectName(String projectName);
}
