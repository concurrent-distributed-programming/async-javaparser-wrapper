package app.view.swing;

import app.model.FileTreeNodeIcon;
import app.model.FileTreeNodeImpl;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;
import app.view.swing.jTree.JTreeBuilding;
import app.view.swing.jTree.JTreeBuildingImpl;
import app.view.swing.jTree.TreeNodeCellRenderer;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;

public class VisualiserPanel extends JPanel implements Panel {
    private final JTreeBuilding jTreeBuilder;
    private final DefaultTreeModel treeModel;

    public VisualiserPanel() {
        this.setLayout(new GridLayout());
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        requestFocusInWindow();

        DefaultMutableTreeNode projectReportNode = new DefaultMutableTreeNode(
            new FileTreeNodeImpl("", FileTreeNodeIcon.PACKAGE)
        );
        treeModel = new DefaultTreeModel(projectReportNode);
        JTree fileTree = new JTree(treeModel);
        this.add(new JScrollPane(fileTree));
        jTreeBuilder = new JTreeBuildingImpl(treeModel);
        fileTree.setCellRenderer(new TreeNodeCellRenderer());
    }

    @Override
    public void addInterface(ClassReport interfaceReport) {
        jTreeBuilder.addInterfaceLeaf(interfaceReport);
    }

    @Override
    public void addClass(ClassReport classReport) {
        jTreeBuilder.addClassLeaf(classReport);
    }

    @Override
    public void addPackage(PackageReport packageReport) {
        jTreeBuilder.addPackageLeaf(packageReport);
    }

    @Override
    public void setProjectName(String projectName) {
        treeModel.setRoot(new DefaultMutableTreeNode(
            new FileTreeNodeImpl(projectName, FileTreeNodeIcon.PACKAGE)
        ));
        treeModel.reload();
    }

}
