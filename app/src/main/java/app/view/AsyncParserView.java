package app.view;

import app.controller.AsyncParserController;
import app.model.pojo.classPojo.ClassReport;
import app.model.pojo.packagePojo.PackageReport;

public interface AsyncParserView {
    void createGUI();

    void addListener(AsyncParserController controller);

    void display();

    void changeState(final String s);

    void addInterface(ClassReport interfaceReport);

    void addClass(ClassReport classReport);

    void addPackage(PackageReport packageReport);
}
